package totoro.unshort

case class Chunk(offset: Int, size: Int, var free: Int, var last: Int, prime: Int) {
  def next(): Int = {
    last = (last + prime) % size
    free -= 1
    last
  }
}
