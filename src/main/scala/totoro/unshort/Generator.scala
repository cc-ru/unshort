package totoro.unshort

object Generator {
  val chunks = Array(
    Chunk(0, 62, 62, 0, 211),
    Chunk(62, 3782, 3782, 0, 5399),
    Chunk(3844, 234422, 234422, 0, 246811),
    Chunk(238328, 14534102, 14534102, 0, 16190227)
  )

  def get(length: Int): Option[String] = {
    if (length > 0 && length <= chunks.length) {
      if (chunks(length - 1).free > 0)
        Some(Base62.encode(chunks(length - 1).next()))
      else None
    } else None
  }
}
