package totoro.unshort

object Base62 {
  def encode(value: Int): String = {
    var result = ""
    var x = value
    while (x > 0) {
      result += char(x % 62)
      x /= 62
    }
    result
  }

  private def char(code: Int): Char = {
    if (code < 10) (code + 48).toChar
    else if (code < 36) (code + 55).toChar
    else (code + 61).toChar
  }
}
