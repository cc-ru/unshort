## unshort
Example:
```sh
$ java -jar ./unshort.jar
New code(1): Some(P)
New code(2): Some(5Q)
New code(3): Some(pD3)
New code(4): Some(hpw6)
```
